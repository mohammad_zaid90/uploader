'use strict'

// Defining and Regiester the metadata file schema
const Schema = mongoose.Schema;

const metadataFileSchema = new Schema({
    recievedFileName: {
        type: String,
        required: true
    },
    bucket: {
        type: String,
        required: true
    },
    key: {
        type: String,
        required: true
    }
}, { timestamps: true });

// Define a model
mongoose.model('metadataFile', metadataFileSchema);

const metadataFileCollection = db.model('metadataFile');

module.exports = metadataFileCollection;