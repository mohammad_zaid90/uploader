'use strict'

//Defining and Regiester the images schema
const Schema = mongoose.Schema;

const imagesSchema = new Schema({
    recievedFileName: {
        type: String,
        required: true
    },
    bucket: {
        type: String,
        required: true
    },
    key: {
        type: String,
        required: true
    },
    size: {
        type: Number
    },
    status: {
        type: String,
        required: true,
        enum: ['good', 'bad', 'inProcess'],
        default: 'inProcess'
    }
}, { timestamps: true });

//Creating indexs to improve the retrive performance
imagesSchema.index({ recievedFileName: -1 });
imagesSchema.index({ status: -1 });

// Define a model
mongoose.model('images', imagesSchema);

const imageCollection = db.model('images');

module.exports = imageCollection;