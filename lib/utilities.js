'use strict';

/**
 * Exponential backoff timeout  
 * @param {Number} retryCount 
 * @param {Number} factor 
 * @param {Number} maxBackoffTime 
 * @returns {Number} - timeout in milliseconds
 */
function exponentialBackoff(retryCount, factor, maxBackoffTime) {
    const exponentialBackoffTime = factor * Math.pow(2, retryCount);
    return (exponentialBackoffTime < maxBackoffTime && exponentialBackoffTime) || maxBackoffTime;
}

function wait(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = {
    wait,
    exponentialBackoff
};