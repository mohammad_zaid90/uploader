'use strict';

const AWS = require('aws-sdk');

class awsHandler {

    constructor() {
        // Singleton
        if (this.services) return this;

        this.services = {};
    }

    /**
     * Create AWS service by service name and region
     * @param {string} serviceName - AWS service name
     * @param {string} region - Aws region
     * @param {Object|null|undefined} serviceOptions 
    */
    createAwsService(serviceName, region, serviceOptions) {
        const self = this;

        if (self.services[serviceName]) return self.services[serviceName];

        const awsService = AWS[serviceName];

        if (!awsService) throw `Unknown AWS service requested: ${serviceName}`;

        const awsConfig = {
            region
        };

        if (serviceOptions) {
            Object.assign(awsConfig, serviceOptions);
        }
        self.services[serviceName] = new awsService(awsConfig);

        return self.services[serviceName];
    }
}

module.exports = awsHandler;