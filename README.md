# intel_uploader

## This service provides:
Rest api for uploading images and their metadat to AWS s3 and DB.   

## __REST API__
Consists of the following:

### POST /files/upload - upload files to s3, store AWS s3 keys to DB

## Configurations
Can be set using environment variables. Otherwise defaults are used:
1. DB_CONNECTION_STRING (**Required**): Db connection string
2. PORT (**Optional**): Server listening port