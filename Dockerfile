#
# uploader
#
FROM node:10-alpine

LABEL maintainer="Mohammad Zaid"

RUN apk update

# Create app directory
WORKDIR /usr/src/app

# Copy package to WORKDIR
COPY package.json package-lock.json ./

# Install app dependencies
RUN npm install

# Copy app source
COPY . .

# Run main backend
CMD [ "node", "index.js" ]