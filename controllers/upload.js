'use strict';

const multer = require('multer'),
    multerS3 = require('multer-s3'),
    responseErrors = require('http-errors');

const awsHandlerClass = require('../lib/awsHandlerClass'),
    imageCollection = require('../schemas/image'),
    metadataFileCollection = require('../schemas/metadataFile'),
    utilities = require('../lib/utilities');


class uploadClass {

    constructor() {
        this.awsHandler = new awsHandlerClass();
        this.S3 = this.awsHandler.createAwsService('S3', 'eu-central-1');
        this.maxBackoffTime = 1000 * 60;
    }

    /**
     * Upload req.files to the provided bucket name
     * @param {string} bucketName 
     */
    upload(bucketName) {
        const self = this;
        return multer({
            storage: multerS3({
                s3: self.S3,
                bucket: bucketName,
                key: (req, file, cb) => {
                    var datenow = Date.now();
                    var fileName = file.originalname || (datenow + "." + file.mimetype.split('/')[1]);
                    cb(null, fileName);
                }
            })
        });
    }

    handleUploadedFiles(req, res, next) {
        const self = this;
        const uploadedFilesData = req.files;

        if (!uploadedFilesData) return next(new responseErrors.BadRequest('Invalid files'));

        // Prepare handler promises for uploaded files
        const handlePromises = uploadedFilesData.map(file => {

            // File original name would be equal to fileName.extension (e.g file.jpeg)
            const fileNameAndExtension = file.originalname && file.originalname.split('.');

            if (!fileNameAndExtension) {
                logger.error(`[UploadCtrl] Invalid file original name ${file.originalname}`);
                return;
            }
            const fileExtension = fileNameAndExtension[1];
            file.fileName = fileNameAndExtension[0];

            switch (fileExtension) {
                case 'jpeg':
                case 'jpg':
                case 'png':
                    return self.handleUploadedFileWithRetry(self.insertUploadedFile.bind(null, imageCollection, file))
                        .catch(err => {
                            logger.error(`[UploadCtrl] Failed to insert the uploaded image file to DB ${file}. ${err}`);
                        });
                case 'json':
                    return self.handleUploadedFileWithRetry(self.insertUploadedFile.bind(null, metadataFileCollection, file))
                        .catch(err => {
                            logger.error(`[UploadCtrl] Failed to insert the uploaded image metadata file to DB ${file}. ${err}`);
                        });
                default:
                    logger.warn(`[UploadCtrl] Unsupported file type ${fileExtension}`);
                    return;
            }
        });

        Promise.all(handlePromises)
            .then(() => {
                const filesOriginalNames = uploadedFilesData.map(file => file.originalname);
                logger.info(`[UploadCtrl] Files were inserted to DB ${filesOriginalNames}`);
                res.send(filesOriginalNames);
            })
    }

    insertUploadedFile(collection, file) {
        const { fileName, bucket, key, size } = file;

        return collection.updateOne({ recievedFileName: fileName }, { bucket, key, size }, { upsert: true, new: true, setDefaultsOnInsert: true });
    }

    /**
     * Call the provided function, retry on error until max retries reached
     * @param {function} handleFunc 
     */
    async  handleUploadedFileWithRetry(handleFunc) {
        const self = this;
        let retryCount = 0;
        do {
            try {
                const result = await handleFunc.call();
                return result;
            }
            catch (error) {
                if (++retryCount > maxRetryCount) throw `[UploadCtrl] Cannot perform function ${handleFunc.name}: too many retries`;
                logger.error(`[UploadCtrl] Cannot perform function ${handleFunc.name}: ${JSON.stringify(error)}`);

                await utilities.wait(utilities.exponentialBackoff(retryCount, config.exponentialBackoffFactorMs, self.maxBackoffTime));
            }
        } while (true);
    }
}

module.exports = uploadClass;