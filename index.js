
global.logger = console;

const express = require('express'),
    config = require('config'),
    bodyParser = require('body-parser'),
    errorhandler = require('errorhandler');

mongoose = require('mongoose');

//initilize express 
app = express();
db = null;

// Uncaughts handlers
process.on('uncaughtException', err => {
    logger.error('Uncaught exception', err, err.stack);
    process.exit(2);
});
process.on('unhandledRejection', (reason, badPromise) => {
    logger.error(`Unhandled rejection at ${reason.stack}`);
    process.exit(3);
});

// parse application/json
app.use(bodyParser.json());

// Error handler
app.use((err, req, res, next) => {
    const returnedError = err || 'Unspecified error!';

    // Add error message to response so we can log it later
    res.errorMsg = returnedError;

    // We shouldn't get here
    if (!err) {
        return res.status(500).send('Unspecified error!')
    }

    return res.status(err.status || 400).send(err.toString() || err);
});

app.use(errorhandler({ dumpExceptions: true, showStack: true }));
app.use(bodyParser.json({ defer: true, limit: '50mb' }));

// Create DB connection
mongoose.connect(config.dbConnectionString, { useUnifiedTopology: true })
    .then(connectedDb => {
        db = connectedDb;

        // Init routes
        require('./routes/upload').uploadRoutes();

        return app.listen(config.port);
    })
    .then(server => {

        logger.info(`Intel file system is now up and listen to port ${config.port}`);
    })
    .catch(err => {
        logger.error(`Failed while start the system. ${err}`);
    });
