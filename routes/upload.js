'use strict';

const config = require('config');
const uploadClass = require('../controllers/upload');

exports.uploadRoutes = () => {

    const uploadCtrl = new uploadClass();
    const uploadToS3 = uploadCtrl.upload(config.s3BuckerName);

    app.post('/files/upload', uploadToS3.array('file', config.maxUploadFiles), (req, res, next) => {
        uploadCtrl.handleUploadedFiles(req, res, next);
    });
}